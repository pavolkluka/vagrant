#!/bin/bash
# ########################################################
#  Version:     1.1
#  Date:        2018/03/20
#  Platforms:   Linux
# ########################################################

# SCRIPT VARIABLES
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# BIN VARIABLES
BIN_RM="$( which rm )"
BIN_MV="$( which mv )"
BIN_TR="$( which tr )"
BIN_CAT="$( which cat )"
BIN_GREP="$( which grep )"
BIN_AWK="$( which awk )"
BIN_TEE="$( which tee )"
BIN_CP="$( which cp )"
BIN_MKDIR="$( which mkdir )"
BIN_CHOWN="$( which chown )"
BIN_CHMOD="$( which chmod )"
BIN_WGET="$( which wget )"
BIN_MOUNT="$( which mount )"
BIN_UMOUNT="$( which umount )"
BIN_IP="$( which ip )"
BIN_TAR="$( which tar )"
BIN_SED="$( which sed )"
BIN_FIND="$( which find )"

# DIRECTORY VARIABLES
DIR_MOLOCH="/opt/moloch"
DIR_MOLOCH_INSTALL="$DIR_SCRIPT"

# FILE VARIABLES
FILE_ELASTIC_HEALTH="$( mktemp )"

echo "[MOLOCH] Install some packages."
sudo apt-get install -y gcc libpcre3-dev zlib1g-dev libluajit-5.1-dev libpcap-dev openssl libssl-dev libnghttp2-dev libdumbnet-dev bison flex libdnet autoconf libtool

cd $DIR_MOLOCH_INSTALL
echo "[MOLOCH] Clone MOLOCH Github Repo to $DIR_MOLOCH_INSTALL."
git clone https://github.com/aol/moloch

# Elasticsearch stuff
$BIN_WGET -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/oss-7.x/apt stable main" | sudo $BIN_TEE /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install -y elasticsearch-oss
ES_VER="$(dpkg -l | $BIN_GREP elasticsearch-oss | $BIN_AWK '{print $3}')"
echo "[MOLOCH] Installed Elasticsearch v$ES_VER."

echo "[MOLOCH] Enable Elasticsearch service on boot and start it."
sudo systemctl enable elasticsearch
sudo systemctl start elasticsearch
STAT_ELASTIC="$(systemctl status elasticsearch.service | $BIN_AWK '/Active:/ {print $3}'|tr -d '(|)')"
while [[ "$STAT_ELASTIC" != "running" ]]
do
    echo "[MOLOCH] Waiting for elasticsearch.service ($STAT_ELASTIC)."
    sleep 5
    STAT_ELASTIC="$(systemctl status elasticsearch.service | $BIN_AWK '/Active:/ {print $3}'|tr -d '(|)')"
done
sudo systemctl stop elasticsearch
systemctl status elasticsearch

echo "[MOLOCH] Install PIP3"
sudo apt-get -y install python3-pip

echo "[MOLOCH] Building capture"
cd $DIR_MOLOCH_INSTALL/moloch
sudo ./easybutton-build.sh --dir $DIR_MOLOCH --pfring --daq --install

echo "[MOLOCH] Create variables for moloch configuration."
MOLOCH_INTERFACE="$($BIN_IP addr | $BIN_GREP -E '^2:' | $BIN_GREP -Eo '[a-z]{3}[0-9]{1}')"
MOLOCH_LOCALELASTICSEARCH="yes"
MOLOCH_ELASTICSEARCH='http://127.0.0.1:9200'
MOLOCH_PASSWORD="admin"
MOLOCH_INET="yes"

echo "[MOLOCH] Make backup of $DIR_MOLOCH/bin/Configure."
sudo $BIN_CP -v $DIR_MOLOCH/bin/Configure $DIR_MOLOCH/bin/Configure.orig
echo "[MOLOCH] Prepare moloch $DIR_MOLOCH/bin/Configure for make install."
sudo $BIN_SED \
-e 's?read -r MOLOCH_INTERFACE?MOLOCH_INTERFACE='${MOLOCH_INTERFACE}'?g' \
-e 's?read -r MOLOCH_LOCALELASTICSEARCH?MOLOCH_LOCALELASTICSEARCH='${MOLOCH_LOCALELASTICSEARCH}'?g' \
-e 's?read -r MOLOCH_PASSWORD?MOLOCH_PASSWORD='${MOLOCH_PASSWORD}'?g' \
-e 's?MOLOCH_ELASTICSEARCH="http://localhost:9200"?MOLOCH_ELASTICSEARCH='${MOLOCH_ELASTICSEARCH}'?g' \
-e 's?read -r MOLOCH_INET?MOLOCH_INET='${MOLOCH_INET}'?g' \
-e 's?ES_VERSION=7.7.1?ES_VERSION='${ES_VER}'?g' \
sudo $DIR_MOLOCH/bin/Configure.orig | sudo $BIN_TEE $DIR_MOLOCH/bin/Configure.new
sudo $BIN_CP -v $DIR_MOLOCH/bin/Configure.new $DIR_MOLOCH/bin/Configure

sudo $BIN_CHMOD 0755 $DIR_MOLOCH/bin/Configure

echo "[MOLOCH] Try to make config."
sudo make config $DIR_MOLOCH/bin/Configure

echo "[MOLOCH] Enable Moloch Viewer and Elasticsearch on boot."
sudo systemctl enable molochviewer.service
sudo systemctl enable elasticsearch.service
echo "[MOLOCH] Disable Moloch Capture on boot."
sudo systemctl disable molochcapture.service && sudo systemctl stop molochcapture.service

echo "[MOLOCH] Starting & preparing Elasticsearch."
sudo systemctl start elasticsearch.service

STAT_ELASTIC="$(systemctl status elasticsearch.service | $BIN_AWK '/Active:/ {print $3}'|tr -d '(|)')"
while [[ "$STAT_ELASTIC" != "running" ]]
do
    echo "[MOLOCH] Waiting for elasticsearch.service."
    sleep 2
    STAT_ELASTIC="$(systemctl status elasticsearch.service | $BIN_AWK '/Active:/ {print $3}'|tr -d '(|)')"
done

echo "[MOLOCH] Elasticsearch service: $STAT_ELASTIC."
echo "[MOLOCH] Init Database."
$BIN_WGET -q http://127.0.0.1:9200/_cluster/health -O $FILE_ELASTIC_HEALTH
STAT_DB=$($BIN_GREP -oP "(?<=status\"\:\").*?(?=\")" $FILE_ELASTIC_HEALTH)
while [[ "$STAT_DB" != "green" ]]
do
    echo "[MOLOCH] Waiting for Elasticsearch Database status."
    sleep 2
    $BIN_WGET -q http://127.0.0.1:9200/_cluster/health -O $FILE_ELASTIC_HEALTH
    STAT_DB=$($BIN_GREP -oP "(?<=status\"\:\").*?(?=\")" $FILE_ELASTIC_HEALTH)
done

sudo $DIR_MOLOCH/db/db.pl http://127.0.0.1:9200 init

echo "[MOLOCH] Create admin account for Moloch."
sudo $DIR_MOLOCH/bin/moloch_add_user.sh admin "Admin User" "admin" --admin

echo "[MOLOCH] Configure Moloch WISE Plugin."
sudo $DIR_MOLOCH/bin/Configure --wise
sudo systemctl enable molochwise.service

echo "[Edit] MOLOCH Moloch service to waiting for Elasticsearch service."
sudo $BIN_MV -v /etc/systemd/system/molochviewer.service /etc/systemd/system/molochviewer.service.orig
sudo $BIN_CAT > /etc/systemd/system/molochviewer.service <<EOL
[Unit]
Description=Moloch Viewer
After=network.target elasticsearch.service
Requires=network.target elasticsearch.service

[Service]
Type=simple
Restart=on-failure
StandardOutput=tty
EnvironmentFile=-/opt/moloch/etc/molochviewer.env
ExecStart=/bin/sh -c 'sleep 5 && /opt/moloch/bin/node viewer.js -c /opt/moloch/etc/config.ini ${OPTIONS} >> /opt/moloch/logs/viewer.log 2>&1'
WorkingDirectory=/opt/moloch/viewer

[Install]
WantedBy=multi-user.target

EOL
sudo $BIN_CHMOD 0644 /etc/systemd/system/molochviewer.service

echo "[MOLOCH] Create script for processing PCAP files."
sudo $BIN_CAT > /etc/cron.d/pcap-upload.bash <<EOL
#!/bin/bash
# ########################################################
#  Version:     1.0
#  Date:        2019/01/31
#  Platforms:   Linux
# ########################################################

# SCRIPT VARIABLES
SCRIPT_DIR="\$( cd "\$( dirname "\${BASH_SOURCE[0]}" )" && pwd )"

# BIN VARIABLES
BIN_MV="\$( which mv )"
BIN_CAT="\$( which cat )"
BIN_GREP="\$( which grep )"
BIN_PGREP="\$( which pgrep )"
BIN_AWK="\$( which awk )"
BIN_CP="\$( which cp )"
BIN_MKDIR="\$( which mkdir )"
BIN_FIND="\$( which find )"
BIN_CHOWN="\$( which chown )"
BIN_LOGGER="\$( which logger )"

# LOG VARIABLES
LOG_SCRIPT_NAME="\$( basename \$0 )"

# DIRECTORY VARIABLES
DIR_PCAP_NEW="/vagrant/share/pcap/new"
DIR_PCAP_READY="/vagrant/share/pcap/ready"

# FILE VARIABLES
FILE_PCAP_LIST="\$( mktemp )"

\$BIN_LOGGER -sp user.info -t \$LOG_SCRIPT_NAME "PCAP-UPLOAD: Check if share folder exist."
if [ -d "/vagrant/share" ]
then
   if [ ! -d "\$DIR_PCAP_READY" ] || [ ! -d "\$DIR_PCAP_NEW" ]
   then
       \$BIN_MKDIR -pv "\$DIR_PCAP_NEW"
       \$BIN_MKDIR -pv "\$DIR_PCAP_READY"
       \$BIN_CHOWN -R vagrant /vagrant/share
   else
       \$BIN_LOGGER -sp user.info -t \$LOG_SCRIPT_NAME "PCAP-UPLOAD: Share folders exists."
   fi
else
   \$BIN_LOGGER -sp user.info -t \$LOG_SCRIPT_NAME "PCAP-UPLOAD: Share folders doesn't exist. Folder was created."
   \$BIN_MKDIR -pv "\$DIR_PCAP_NEW"
   \$BIN_MKDIR -pv "\$DIR_PCAP_READY"
   \$BIN_CHOWN -R vagrant /vagrant/share
fi

if [[ \$(\$BIN_PGREP -f \$LOG_SCRIPT_NAME) ]]
then
   \$BIN_FIND \$DIR_PCAP_NEW -type f -exec file {} \; | \$BIN_AWK -F ':' '/pcap/ {print \$1}' > \$FILE_PCAP_LIST
   while read PCAP
   do
       PCAP_FILE="\$(echo \$PCAP | \$BIN_AWK -F '/' '{print \$NF}')"
       \$BIN_LOGGER -sp user.info -t \$LOG_SCRIPT_NAME "PCAP-UPLOAD: Processing file \$DIR_PCAP_READY/\$PCAP_FILE."
       \$BIN_MV -v \$PCAP \$DIR_PCAP_READY/
       /opt/moloch/bin/moloch-capture -r \$DIR_PCAP_READY/\$(echo \$PCAP | \$BIN_AWK -F '/' '{print \$NF}')
       \$BIN_CHOWN -R vagrant:vagrant \$DIR_PCAP_READY
       \$BIN_LOGGER -sp user.info -t \$LOG_SCRIPT_NAME "PCAP-UPLOAD: File \$DIR_PCAP_READY/\$PCAP_FILE is ready."
    done < \$FILE_PCAP_LIST
else
   \$BIN_LOGGER -sp user.info -t \$LOG_SCRIPT_NAME "PCAP-UPLOAD: is running."
fi

EOL

sudo $BIN_CHMOD +x /etc/cron.d/pcap-upload.bash

echo "[MOLOCH] Add script to root crontab."
sudo echo "*/1 * * * * root /etc/cron.d/pcap-upload.bash" >> /etc/crontab

echo "[MOLOCH] Make backup of /opt/moloch/etc/config.ini."
sudo $BIN_MV -v $DIR_MOLOCH/etc/config.ini $DIR_MOLOCH/etc/config.ini.orig
MOLOCH_CONFIG_PCAPDIR="/vagrant/share/pcap"
MOLOCH_CONFIG_USER="vagrant"
MOLOCH_CONFIG_GROUP="vagrant"
echo "[MOLOCH] Change pcapDir, dropUser, dropGroup in /opt/moloch/etc/config.ini."
$BIN_SED \
-e 's?pcapDir = /opt/moloch/raw?pcapDir = '${MOLOCH_CONFIG_PCAPDIR}'?g' \
-e 's?dropUser=nobody?dropUser='${MOLOCH_CONFIG_USER}'?g' \
-e 's?dropGroup=daemon?dropGroup='${MOLOCH_CONFIG_GROUP}'?g' \
$DIR_MOLOCH/etc/config.ini.orig |sudo $BIN_TEE $DIR_MOLOCH/etc/config.ini

echo "[MOLOCH] Reload daemon for edited services (Viewer, WISE)."
sudo systemctl daemon-reload

echo "[MOLOCH] Start Moloch Viewer service."
sudo systemctl start molochviewer.service

# echo "[MOLOCH] Start Moloch Wise service."
# sudo systemctl start molochwise.service

echo "[MOLOCH] Get status of services."
systemctl status elasticsearch.service
systemctl status molochviewer.service
systemctl status molochwise.service

echo "[MOLOCH] Well done."

exit 0
